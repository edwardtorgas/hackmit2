    Aaron (given name)
    Abagor
    Abamon
    Abataly
    Abdaikl
    Abdullah (name)
    Abel (given name)
    Abelyar
    Abid
    Abily
    Abnody
    Abo (name)
    Abram (name)
    Aburom
    Afanasy
    Agafangel
    Agafodor
    Agafon
    Agafonik
    Agap
    Agapion
    Agapit
    Agapy
    Agat (given name)
    Agav
    Agavva
    Alexey
    Amvrosy
    Anatoly
    Andrey
    Anton (given name)
    Artemy
    Artyom
    Avda (given name)
    Avdakt
    Avdelay
    Avdey
    Avdifaks
    Avdiky
    Avdiyes
    Avdon (given name)
    Avel
    Avenir (given name)
    Aventin (given name)
    Averky
    Avessalom
    Avgar
    Avgury
    Avgust
    Avgustin
    Avian (given name)
    Avim (given name)
    Avimelekh
    Avip
    Avit
    Aviv
    Avksenty
    Avksily
    Avksivy
    Avkt
    Avram (given name)
    Avrelian
    Avreliya
    Avrely
    Avrey
    Avros
    Avsey
    Avtonom
    Avudim
    Avundy
    Avva
    Avvakir
    Avvakum (given name)

B

    Bogdan
    Boris (given name)
    Boyan (given name)
    Branislav

D

    Danilo
    Damir
    Daniil
    Dmitry

G

    Gennady
    Genrikh
    Georgy (name)
    Gerasim
    Gleb (name)
    Gniewomir
    Grigory
    Grischa

I

    Igor (given name)
    Ilarion
    Ilya
    Inal (name)
    Ivan (name)

J

    Jaroslav

K

    Kirill
    Konstantin
    Konstantine

L

    Leonid
    Lubomir
    Ludomir
    Lukyan

M

    Maxim (given name)
    Mikhail
    Milan (given name)
    Milorad
    Miroslav (given name)
    Mstislav (given name)

N

    Nail (given name)
    Nikita
    Nikita (given name)

O

    Oleg
    Osip
    Ossip

P

    Panteley
    Pavel
    Pavsikakiy
    Petya
    Piotr
    Pyotr
 Radomir (given name)
    Radoslav
    Radul
    Rasim
    Ratimir
    Roman (name)
    Rostislav (given name)
    Ruslan (given name)
    Rustem


    Sambor
    Sasho
    Sobieslaw
    Stanimir
    Stanislav (given name)
    Svetoslav
    Sviatoslav


    Timofey
    Timur (name)

V

    Vadim (name)
    Valery
    Vanya
    Velimir
    Veniamin
    Viacheslav
    Viktor
    Vitomir
    Vladan
    Vladimir (name)
    Vladislav
    Vojislav
    Volodymyr (name)
    Vsevolod
    Vsevolod (given name)


    Yefim
    Yegor
    Yermolay
    Yury

    Zinoviy
    Zinovy